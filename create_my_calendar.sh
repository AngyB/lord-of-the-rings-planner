# this script generates a monthly calendar
# in order to generate the calendar for another month
# change the arguments of create_calendar.sh script

# =============change this===============
starting_day=Monday # the day of the starting date. For instance in
# 2021 November 1st is a Monday 
month_num=10 # for November
starting_date=1 #I want to start the calendar from the 1st of the month
year=2024 # number of year
season=autumn # I want the seasonal theme of this month to be automn
#=================themes=================
# autumn = Inspiration: human race -> Gondor
# winter = Inspiration dwarves -> Moria
# spring = Inspiration hobbits -> Hobbiton
# summer = Inspiration elves -> Rivendel
#========================================
cd day
    # [number of months you want to generate] [starting day] [starting date] [seasonal theme]
    bash create_calendar.sh 1 $starting_day $starting_date $month_num $season
cd ..

cd month
    bash create_calendar.sh 1 $starting_day $starting_date $month_num $year $season
cd ..
# =========================================



pdftk day/$season-empty.pdf cat 2 output lol1.pdf
pdftk month/month.pdf cat 1-2 output lol2.pdf
pdftk lol1.pdf lol2.pdf cat output lol3.pdf

var=$(pdftk day/day-new.pdf dump_data | grep NumberOfPages | awk '{print $2}')

pdftk day/day-new.pdf cat 1-$var output lol1.pdf
pdftk lol3.pdf lol1.pdf cat output lol2.pdf


if ! ((var % 4))
then
    pdftk day/$season-empty.pdf cat 1 output lol1.pdf
else
    pdftk day/$season-empty.pdf cat 1-2 output lol1.pdf
fi
pdftk lol2.pdf lol1.pdf cat output monthly_calendar.pdf



rm -f lol1.pdf lol2.pdf lol3.pdf tmp.pdf

