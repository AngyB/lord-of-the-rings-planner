# [number of months] [day of the week <String>] [the date] [number of month 8=August] [season <String>]
# bash create_calendar.sh 3 Wednesday 1 9 autumn
# starting date
declare -A days
days=(["Monday"]=1 ["Tuesday"]=2 ["Wednesday"]=3 ["Thursday"]=4 ["Friday"]=5 ["Saturday"]=6 ["Sunday"]=7)
echo ${days["$2"]}


DAY=${days["$2"]}
DATE=$3
MONTH=$4

# how many months you want to generate
END=$1
END="$(($END+1))"
# change the number of months to be generated
# in file month1
file="day-new.tex"

var=$(awk '/\setcounter{numberofmonths}/{print;exit}' $file)
sed -i "s/\\$var/\\\setcounter{numberofmonths}{$END}/" $file
# change daycount
var=$(awk '/setcounter{daycount}/{print;exit}' $file)
sed -i "s/\\$var/\\\setcounter{daycount}{$DATE}/" $file
# change day
var=$(awk '/setcounter{dispdaycount}/{print;exit}' $file)
sed -i "s/\\$var/\\\setcounter{dispdaycount}{$DAY}/" $file
# change month
var=$(awk '/setcounter{monthcount}/{print;exit}' $file)
sed -i "s/\\$var/\\\setcounter{monthcount}{$MONTH}/" $file
# change season 
var=$(awk '/includegraphics/{print;exit}' $file)
echo $var
var_inv=$(awk '/-inv/{print;exit}' $file)
echo $var
var="${var#*img/}"
var_inv="${var_inv#*img/}"
echo $var
echo $var_inv
sed -i "s/$var/$5.png}/" $file
sed -i "s/$var_inv/$5-inv.png}/" $file

xelatex $file

xelatex $file

xelatex $file

# remove useless files

rm -f *.aux *.log *.fdb_latexmk *.fls

