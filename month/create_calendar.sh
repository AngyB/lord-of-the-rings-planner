# [number of months] [day of the week <String>] [the date] [number of month 8=August] [year] [season all small <String>]
# bash create_calendar.sh 3 Wednesday 1 9 autumn
# starting date
declare -A days
days=(["Monday"]=1 ["Tuesday"]=2 ["Wednesday"]=3 ["Thursday"]=4 ["Friday"]=5 ["Saturday"]=6 ["Sunday"]=7)

Season=$6

if ((${days["$2"]} > 3))
then
    day2=${days["$2"]}
    date2=$3

    day1=1
    tmp=$((8-${days["$2"]}))
    date1=$(($3+$tmp))

    
else
    day1=${days["$2"]}
    date1=$3

    day2=4
    tmp=$((4-${days["$2"]}))
    date2=$(($3+$tmp))

    
fi

echo "date1 = " $date1
    echo "day1 = " $day1

tmp=$(($5-2020))
leap_year=$(($tmp % 4))

echo "LEAP YEAR" $leap_year
# how many months you want to generate
END=$1
END="$(($END+1))"
# change the number of months to be generated
# in file month1
month1="month1/month1-new.tex"

var=$(awk '/\setcounter{numberofmonths}/{print;exit}' $month1)
sed -i "s/\\$var/\\\setcounter{numberofmonths}{$END}/" $month1
# change daycount
var=$(awk '/setcounter{daycount}/{print;exit}' $month1)
sed -i "s/\\$var/\\\setcounter{daycount}{$date1}/" $month1
# change day
var=$(awk '/setcounter{weekdayenum}/{print;exit}' $month1)
sed -i "s/\\$var/\\\setcounter{weekdayenum}{$day1}/" $month1
# change month
var=$(awk '/setcounter{monthcount}/{print;exit}' $month1)
sed -i "s/\\$var/\\\setcounter{monthcount}{$4}/" $month1
# change year
var=$(awk '/setcounter{yearcount}/{print;exit}' $month1)
sed -i "s/\\$var/\\\setcounter{yearcount}{$5}/" $month1
# change season 
var=$(awk '/TileWallPaper/{print;exit}' $month1)
var="${var#*m1}"
sed -i "s/$var/$Season-mirror.png}/" $month1

# in file month2
month2="month2/month2-new.tex"

var=$(awk '/\setcounter{numberofmonths}/{print;exit}' $month2)
sed -i "s/\\$var/\\\setcounter{numberofmonths}{$END}/" $month2
# change daycount
var=$(awk '/setcounter{daycount}/{print;exit}' $month2)
sed -i "s/\\$var/\\\setcounter{daycount}{$date2}/" $month2
# change day
var=$(awk '/setcounter{weekdayenum}/{print;exit}' $month2)
sed -i "s/\\$var/\\\setcounter{weekdayenum}{$day2}/" $month2
# change month
var=$(awk '/setcounter{monthcount}/{print;exit}' $month2)
sed -i "s/\\$var/\\\setcounter{monthcount}{$4}/" $month2
# change year
var=$(awk '/setcounter{yearcount}/{print;exit}' $month2)
sed -i "s/\\$var/\\\setcounter{yearcount}{$5}/" $month2
# change season 
var=$(awk '/TileWallPaper/{print;exit}' $month2)
var="${var#*m2}"
sed -i "s/$var/$Season-mirror.png}/" $month2
# compile latex files

END="$(($END-1))"


xelatex month1/month1-new.tex

xelatex month2/month2-new.tex

# combine the month files
pdftk month1-new.pdf cat 1 output month1.pdf  
pdftk month2-new.pdf cat 1 output month2.pdf  
pdftk month1.pdf month2.pdf cat output month.pdf

for i in $(seq 2 $END)
do
    echo $i
    pdftk month1-new.pdf cat $i output month1.pdf  
    pdftk month2-new.pdf cat $i output month2.pdf  
    pdftk month.pdf month1.pdf cat output tmp.pdf
    pdftk tmp.pdf month2.pdf cat output month.pdf
    
done

# remove useless files
rm -f *.aux *.log tmp.pdf month1.pdf month2.pdf month1-new.pdf month2-new.pdf 

cd month1 
rm -f *.aux *.log *.fdb_latexmk *.fls

cd ../month2 
rm -f *.aux *.log *.fdb_latexmk *.fls

