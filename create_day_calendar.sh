# =============change this===============
generated_months_num=3 # the number of months you want to generate 
starting_day=Monday # the day of the starting date. For instance in
# 2021 November 1st is a Monday 
month_num=11 #for November
starting_date=1 #I want to start the calendar from the 1st of the month
season=winter # I want the seasonal theme of this month to be automn
#=================themes=================
# autumn = Inspiration: human race -> Gondor
# winter = Inspiration dwarves -> Moria
# spring = Inspiration hobbits -> Hobbiton
# summer = Inspiration elves -> Rivendel
#========================================
cd day
    # [number of months you want to generate] [starting day] [starting date] [seasonal theme]
    bash create_calendar.sh $generated_months_num $starting_day $starting_date $month_num $season
cd ..

pdftk day/$season-empty.pdf cat 2 output lol1.pdf
pdftk lol1.pdf day/day-new.pdf cat output day_callendar.pdf

rm -f lol1.pdf lol2.pdf